function clientCustomEvents() {
    this.sendMessageButton = document.getElementById("customMessageButton");
    this.serverStatus = document.getElementById("serverStatus");
    this.messagesBoard = document.getElementById("messagesBoard");
    this.content = document.getElementById("content");
    this.error = document.getElementById("error");

    this.createCustomEvents = () => {
        window.addEventListener('listen', (e) => {
            this.clientMessagesEvent(e)
        }, false);
    }
    this.createCustomEvents();

    this.clientMessagesEvent = (e) => {
        this.messagesBoard.innerHTML = this.messagesBoard.innerHTML
            + "\n" + e.detail;
    }

    this.sendMessageToServer = () => {
        const event = new CustomEvent('listen', { detail: "custom message from client" });
        opener.dispatchEvent(event);
    }

    assignEvents = () => {
        this.sendMessageButton.onclick = this.sendMessageToServer;
    }

    checkServerAvailabilityLoop = () => {
        setTimeout(() => {
            if (!opener || opener.closed) {
                this.content.style.display="none";
                this.error.style.display="block";
            } else {
                this.content.style.display="block";
                this.error.style.display="none";
            }
            checkServerAvailabilityLoop();
        }, 500);
    }

    assignEvents();
    checkServerAvailabilityLoop();
}