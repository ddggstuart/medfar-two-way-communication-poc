function handShake() {
    this.openButton = document.getElementById("openButton");
    this.closeButton = document.getElementById("closeButton");
    this.sendMessageButton = document.getElementById("customMessageButton");
    this.clientStatus = document.getElementById("clientStatus");
    this.messagesBoard = document.getElementById("messagesBoard");
    this.clientWindow = null;

    this.createCustomEvents = () => {
        window.addEventListener('listen', (e) => {
            this.serverMessagesEvent(e)
        }, false);
    }
    this.createCustomEvents();

    this.openClient = () => {
        if (this.clientWindow)
            return;
        
        this.clientWindow = window.open("./client.html");
    }

    this.serverMessagesEvent = (e) => {
        this.messagesBoard.innerHTML = this.messagesBoard.innerHTML
            + "\n" + e.detail;
    }

    this.sendMessageToClient = () => {
        const event = new CustomEvent('listen', { detail: "custom message from server" });
        this.clientWindow.dispatchEvent(event);
    }

    this.closeClient = () => {
        this.clientWindow.close();
    }

    this.setClientStatus = (state) => {
        this.clientStatus.innerHTML = state;
    }

    assignEvents = () => {
        this.openButton.onclick = this.openClient;
        this.sendMessageButton.onclick = this.sendMessageToClient;
        this.closeButton.onclick = this.closeClient;
    }

    checkClientAvailabilityLoop = () => {
        setTimeout(() => {
            if (this.clientWindow && !this.clientWindow?.closed) {
                this.setClientStatus("Available");
            } else {
                this.setClientStatus("Offline");
                this.clientWindow = null;
            }
            checkClientAvailabilityLoop();
        }, 500);
    }
    assignEvents();
    checkClientAvailabilityLoop();
}