function clientHandShake() {
    this.sendMessageButton = document.getElementById("customMessageButton");
    this.serverStatus = document.getElementById("serverStatus");
    this.messagesBoard = document.getElementById("messagesBoard");
    this.content = document.getElementById("content");
    this.error = document.getElementById("error");
    this.serverMessagesListener = null;

    this.handShake = (serverMessagesListener) => {
        this.serverMessagesListener = serverMessagesListener;
        window.handShake = null;
        return {
            clientMessagesListener: this.clientMessagesListener,
        }
    }
    window.handShake = this.handShake;

    this.clientMessagesListener = (message) => {
        this.messagesBoard.innerHTML = this.messagesBoard.innerHTML
            + "\n" + message;
    }

    this.sendMessageToServer = () => {
        this.serverMessagesListener("custom message from client");
    }

    assignEvents = () => {
        this.sendMessageButton.onclick = this.sendMessageToServer;
    }

    checkServerAvailabilityLoop = () => {
        setTimeout(() => {
            if (!opener || opener.closed) {
                this.content.style.display="none";
                this.error.style.display="block";
            } else {
                this.content.style.display="block";
                this.error.style.display="none";
            }
            checkServerAvailabilityLoop();
        }, 500);
    }

    assignEvents();
    checkServerAvailabilityLoop();
}